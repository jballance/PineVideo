
; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/Video/HWSupport/PineVideo/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Portions Copyright 2019 Jeffrey Lee
; Use is subject to license terms.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:ModHand
        GET     Hdr:Services
        GET     Hdr:ResourceFS
        GET     Hdr:Proc
        GET     Hdr:FSNumbers
        GET     Hdr:NewErrors
        GET     Hdr:VideoDevice
        GET     Hdr:HALEntries
        GET     Hdr:VIDCList
        GET     Hdr:VduExt
        GET     Hdr:PCI
        GET     Hdr:GraphicsV

        GET     DMA.hdr
        GET     VersionASM

        GBLL    Debug
Debug   SETL    {FALSE}

          GBLL  UseGraphV ; Use GraphicsV?
UseGraphV SETL  {TRUE}

          GBLL  BuiltinMDF ; Enable the builtin MDF
BuiltinMDF SETL {TRUE}



; workspace definition
                 ^       0,      wp                              ; Store

;PeriBase
                 #       4           ; io base address

mbram            #       0           ; structure needed for frame buffer descriptor
mbxres           #       4
mbyres           #       4
mbxvres          #       4
mbyvres          #       4
mbpitch          #       4 ; byte pitch
mbbpp            #       4 ; 8/16/32
mbxoff           #       4
mbyoff           #       4
mbbase           #       4 ; physical base of frame buffer
mbscrsz          #       4 ; size

lcdres           #       4           ; LCD Resolution, x + (y<<16)
lcdtest          #       4           ; test
vdu_init         #       4 ; VDU init phys addr, for GV_Render

DMAChan          #       4 ; DMA channel for GraphicsV_Render
DMADesc_Log      #       4 ; Log addr of DMA descriptor buffer
DMADesc_Phys     #       4 ; Phys addr of DMA descriptor buffer
DMA_height       *       1080 ; Max rows to support for DMA rect copy

GVinstance           #   1 ; GraphicsV driver number
GVRenderNotAvailable #   1 ; Whether GraphicsV_Render is available
		     #   2 ; back to word alignment

modedefbuf       #       256

; align to 4 byte boundary
                 #       (((:INDEX:@)+3):AND::NOT:3)-(:INDEX:@)
TotalRAMRequired *      :INDEX: @


; Device-specific struct for the VDU device

                        ^ 0
VDUDevSpec_SizeField    # 4 ; Size field
VDUDevSpec_DMAChan      # 4 ; Pointer to DMA channel
VDUDevSpec_BurstLen     # 4 ; Burst length for use with DMA channel
VDUDevSpec_Size         # 0 ; Size value to write to size field


        ; Assembler modules are conventionally, but not necessarily,
        ; position-independent code. Area name |!| is guaranteed to appear
        ; first in link order, whatever your other areas are named.
        AREA    |!|, CODE, READONLY, PIC

        ENTRY
swi_id  *       0x88000
rsb_hw  *       0x01F03400 ;  0x01F03400
Module_BaseAddr
        DCD     0 ; Start
        DCD     Init - |Module_BaseAddr|
        DCD     Final - |Module_BaseAddr|
        DCD     ServiceCall - |Module_BaseAddr|; Service call handler
        DCD     Title - |Module_BaseAddr|
        DCD     Help - |Module_BaseAddr|
        DCD     0 ; Keyword table
        DCD     0 ;swi_id ; SWI chunk
        DCD     0 ;heck_bat - |Module_BaseAddr|; SWI handler
        DCD     0 ; SWI table
        DCD     0 ; SWI decoder
        DCD     0 ; Messages
        DCD     Flags - |Module_BaseAddr|

Title   =       Module_ComponentName, 0
Help    =       Module_ComponentName, 9, 9, Module_HelpVersion, 0
Flags   &       ModuleFlag_32bit


        ASSERT  Service_PostInit < Service_ModulePostInit
ServiceCallTable
        DCD     0
        DCD     ServiceCallEntry - Module_BaseAddr
        DCD     Service_PostInit
        DCD     0

        DCD     ServiceCallTable - Module_BaseAddr
ServiceCall     ROUT
        MOV     r0, r0
        TEQ     r1, #Service_PostInit
        MOVNE   pc, lr

ServiceCallEntry
      [ BuiltinMDF
        stmfd   sp!, {r0,r2,r12,lr}
        ldr     r12, [r12]
        ldr     r2, lcdres
        ldr     r0, =(768<<16)+1366
        teq     r2, r0
        adreql  r0, loadmodefilecommand14
        adrnel  r0, loadmodefilecommand11
        swi     XOS_CLI
        ldmfd   sp!, {r0,r2,r12,pc}
      |
        MOV     pc, lr
      ]

      [ BuiltinMDF
startupmode14
        DCD 1
        DCD 1366 ;800
        DCD 768 ;600
        DCD 5
        DCD -1
        DCD 0           ; modeflags
        DCD &4000       ; need ltrgb for this
        DCD -1
startupmode11
        DCD 1
        DCD 1920 ;800
        DCD 1080 ;600
        DCD 5
        DCD -1
        DCD 0           ; modeflags
        DCD &4000       ; need ltrgb for this
        DCD -1
      ]

Init    ROUT
        Push    "r8-r9,lr"
        MOV     r0, #ModHandReason_Claim
        LDR     r3, =TotalRAMRequired
        SWI     XOS_Module
        BVS     ExitInitModule

        STR     r2, [r12]
        mov     r12, r2
        MOV     r0, #0

	MOV	r1,#10
	! 0, "Why API version 10?"
	MOV	r0,r1,lsl #16

        MOVW    r1,#HALDeviceType_Video + HALDeviceVideo_VDU
	ORR	r0,r0,r1
 [ Debug
  bl DebugHexTX4
 ]
        MOV	r1,#0
	MOV	r8,#5
	SWI	XOS_Hardware
	;CMP	r1,#-1
	;BEQ	printerror

	ADD	r2, r2,#HALDevice_VDU_Size
	LDR     r0, [r2,#20]         ; DMA channel
	STR     r0, DMAChan
	LDR	r0, [r2,#16]         ; LCD resolution
        str     r0, lcdres

        MOV     r0, #1
        STRB    r0, GVRenderNotAvailable
        MOV     r0, #255
        STRB    r0, GVinstance
        ; Allocate memory needed for DMA descriptors
        LDR     r0, DMAChan
        CMP     r0, #0
        BEQ     %FT10
        LDR     r0, =DMA_DESC_size*DMA_height
        MOV     r1, #0
        MOV     r2, #0
        SWI     XPCI_RAMAlloc
        ; Disable if malloc fails
        MOVVS   r0, #0
        STRVS   r0, DMAChan
        STRVC   r0, DMADesc_Log
        STRVC   r1, DMADesc_Phys
10

 [  UseGraphV
        MOV     r0, #ScreenModeReason_RegisterDriver
        MOV     r1, #0
        ADR     r2, Title
        SWI     XOS_ScreenMode                          ; get a driver number
        BVS     ExitInitModule
        STRB    r0, GVinstance
 ]

        ADRL    R0,resourcefsfiles
        SWI     XResourceFS_RegisterFiles   ; ignore errors

 [ BuiltinMDF
        ldr     r2, lcdres
        ldr     r0, =(768<<16)+1366
        teq     r2, r0

        adreql  r0, loadmodefilecommand14
        adrnel  r0, loadmodefilecommand11
        swi     XOS_CLI
 ]

 [  UseGraphV
        MOV     r0, #GraphicsV                          ; grab GraphicsV
        ADRL    r1, GraphicsV_Handler
        MOV     r2, r12
        SWI     XOS_Claim

        MOV     r0, #ScreenModeReason_StartDriver
        LDRB    r1, GVinstance
        SWI     XOS_ScreenMode              ; let the OS know we're ready
 ]

        Pull    "r8-r9,pc"

ExitInitModule                            ; need to check for what is allocated
        Pull    "r8-r9,pc"

Final   ROUT
        Push    "lr"
        LDR     r12, [r12]

        ADRL    R0,resourcefsfiles
        SWI     XResourceFS_DeregisterFiles ; ignore errors
 [  UseGraphV
        MOV     r0, #ScreenModeReason_StopDriver
        LDRB    r1, GVinstance
        SWI     XOS_ScreenMode              ; tell the OS we're leaving

        MOV     r0, #GraphicsV
        ADRL    r1, GraphicsV_Handler
        MOV     r2, r12
        SWI     XOS_Release

        MOV     r0, #ScreenModeReason_DeregisterDriver
        LDRB    r1, GVinstance
        SWI     XOS_ScreenMode
 ]
        LDR     r0, DMADesc_Log
        CMP     r0, #0
        SWINE   XPCI_RAMFree
        CLRV
        Pull    "pc"


; r0->VIDCList3   (writable!!)
; r12-> workspace
SetModeFromVIDCList     ROUT
        Push    "r0-r7,lr"
        mov     r6, r0                     ; preserve the list pointer
 [ Debug
  bl DebugTXStrInline
  DCB "start SetMode",10,13,0
  ALIGN
 ]

 [  UseGraphV
        ; Block GV_Render, and sync any current transfer for paranoia
        MOV     r0, #1
        STRB    r0, GVRenderNotAvailable
        BL      GV_Render_Sync
 ]
        adrl    r7,modedefbuf
11      ldr     r0, [r6] , #4
        str     r0, [r7] , #4
        cmn     r0, #1
        bne     %bt11
        ldr     r6, [sp]                   ; recover the pointer

 [ Debug
  bl DebugTXStrInline
  DCB 10,13,"modedefbuf printed",10,13,0
  ALIGN
 ]


        LDR     r0, [r6, #VIDCList3_PixelDepth]
 [ Debug
  bl DebugHexTX4
 ]
        CMP     r0, #4
        MOVLO   r0, #8
        MOVEQ   r0, #16
        MOVHI   r0, #32
        STR     r0, mbbpp
;        str     r0, [r3, #dispbpp-tagb]
 [ Debug
  bl DebugHexTX4
 ]
        LDR     r0, [r6, #VIDCList3_HorizDisplaySize]
        STR     r0, mbxres
        STR     r0, mbxvres
;        str     r0, [r3, #phyx-tagb]
;        str     r0, [r3, #virtx-tagb]
 [ Debug
  bl DebugHexTX4
 ]
        LDR     r0, [r6, #VIDCList3_VertiDisplaySize]
        STR     r0, mbyres
        STR     r0, mbyvres
;        str     r0, [r3, #phyy-tagb]
;        str     r0, [r3, #virty-tagb]
 [ Debug
  bl DebugHexTX4
 ]
        mov     r0, r3
 [ Debug
  bl DebugHexTX4
 ]
;        bl      SendMBMessage
 [ Debug
  bl DebugTXStrInline
  DCB "done FB.. size .. physadd",10,13,0
  ALIGN
 ]
 LDR r0,=&49d4000;&3E87D0 ;    300000 ;14000000   ;;;; temop hard code
        str     r0, mbscrsz
 [ Debug
  bl DebugHexTX4
 ]
	MOV	r0,#&be000000
      str     r0, mbbase
        STR     r0, vdu_init ; just in case GV_Render calls are received before RISC OS tells us the VDU address
	 ;bl DebugHexTX4
	[ Debug
	bl DebugTXStrInline
	DCB "mbbase: ",10,13,0
	ALIGN
	]
; [ Debug
;  bl DebugHexTX4
;  mov r0,r6
;  bl DebugHexTX4
;  mov r0,r7
;  bl DebugHexTX4
; ]

;        ldr     r0, [r3,#dispit-tagb]
        ldr     r0, mbbpp
        ldr     r1, mbxres
        mov     r0, r0, lsr #3 ; bits -> bytes
        mul     r0, r1,  r0
        str     r0, mbpitch
 [ Debug
  bl DebugHexTX4
 ]

        ; Allow GV_Render, if DMA channel available
        LDR     r0, DMAChan
        TEQ     r0, #0
        MOVNE   r0, #0
        STRNEB  r0, GVRenderNotAvailable

 [ Debug
  bl DebugTXStrInline
  DCB "done FB2",10,13,0
  ALIGN
 ]

        Pull    "r0-r7,pc"



; r0 = mode or->VIDCList3   (writable!!)
; r12-> workspace
VetModeFromVIDCList     ROUT
        Push    "r0-r7,r12,lr"
        mov     r6, r0                     ; preserve the list pointer
 [ Debug
  bl DebugTXStrInline
  DCB "start vmfv3",10,13,0
  ALIGN
 ]
        adrl    r7,modedefbuf
11      ldr     r0, [r6] , #4
        str     r0, [r7] , #4
 [ Debug
  bl DebugHexTX4
 ]
        cmn     r0, #1
        bne     %bt11
        Pull    "r6"                       ; recover the pointer
 [ Debug
  bl DebugTXStrInline
  DCB 10,13,"modedefbuf printed",10,13,0
  ALIGN
 ]


        ; The VC 'test' calls seem to be broken and just return the current FB settings
        ; So don't bother doing a full vet sequence
        ; However exerimentation suggests that the VC rounds up the display pitch to a multiple of 32 bytes
        ; So as a simple vet, make sure no padding is being introduced, as RISC OS won't expect it
        ; TODO - Should also pay attention to ExtraBytes control list entry
;        LDR     r0, [r6, #VIDCList3_HorizDisplaySize]
;        LDR     r1, [r6, #VIDCList3_PixelDepth]
;        MOV     r0, r0, LSL r1
;        TST     r0, #(32*8)-1
;        BNE     %FT999
        ; check max res doesnt exceed what we can do
 [ Debug
  bl DebugTXStrInline
  DCB "check res fits ",0
  ALIGN
 ]
        ldr     r0, lcdres
 [ Debug
  bl DebugHexTX4
 ]
        bic     r0, r0,#&ff<<16
        bic     r1, r0,#&ff<<24  ; remove y res
        ldr     r0, [r6, #VIDCList3_HorizDisplaySize]
 [ Debug
  bl DebugHexTX4
 ]
        cmp     r1, r0
        blt     %FT999           ; x res too much


 [ Debug
  bl DebugTXStrInline
  DCB "done vFB2",10,13,0
  ALIGN
 ]

        mov     r0, #0
; exit r0=0 if we like the mode, else untouched
        Pull    "r1-r7,r12,pc"
999

 [ Debug
  bl DebugTXStrInline
  DCB "done vFB2 - mode vet fail",10,13,0
  ALIGN
 ]
        MOV     r0, r6                   ; recover original r0
        Pull    "r1-r7,r12,pc"

        LTORG

      [ BuiltinMDF
; make sure the command matches the resource files below!!
loadmodefilecommand11
        DCB     "loadmodefile Resources:$.Resources.PineVideo.PineMon11",0
        ALIGN
loadmodefilecommand14
        DCB     "loadmodefile Resources:$.Resources.PineVideo.PineMon14",0
        ALIGN
      ]


resourcefsfiles
      [ BuiltinMDF
        ResourceFile    Resources.PineMon11, Resources.PineVideo.PineMon11
        ResourceFile    Resources.PineMon14, Resources.PineVideo.PineMon14
      ]
        DCD     0                   ; terminator

 [  UseGraphV
        GET     GraphicsV.s
 ]
        GET     Debug.s

        END
